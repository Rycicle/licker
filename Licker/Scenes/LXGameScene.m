//
//  LXGameScene.m
//  Licker
//
//  Created by Ryan Salton on 18/02/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "LXGameScene.h"

@implementation LXGameScene

-(void)didMoveToView:(SKView *)view {
    /* Setup your scene here */
    
    self.backgroundColor = [UIColor redColor];
    
    self.player.position = CGPointMake(self.size.width * 0.5, self.player.size.height * 0.5);
    [self addChild:self.player];
    
}

- (LXPlayer *)player
{
    if (!_player)
    {
        _player = [[LXPlayer alloc] initWithPlayerType:PlayerTypeFrog andSize:CGSizeMake(self.size.width * 0.6, self.size.width * 0.4)];
    }
    
    return _player;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        
        
    }
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
}

@end
