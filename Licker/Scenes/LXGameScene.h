//
//  LXGameScene.h
//  Licker
//
//  Created by Ryan Salton on 18/02/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "LXPlayer.h"


@interface LXGameScene : SKScene

@property (nonatomic, strong) LXPlayer *player;

@end
