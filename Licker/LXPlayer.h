//
//  LXPlayer.h
//  Licker
//
//  Created by Ryan Salton on 18/02/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>


typedef enum {
    PlayerTypeFrog
} PlayerType;

@interface LXPlayer : SKSpriteNode

@property (nonatomic, assign) PlayerType playerType;

- (instancetype)initWithPlayerType:(PlayerType)playerType andSize:(CGSize)size;

@end
